﻿
namespace FileReader.Infrastructure.Enums
{
    public enum IconSize : short
    {
        Small,
        Large
    }
}
