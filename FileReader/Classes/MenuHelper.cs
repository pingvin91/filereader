﻿using FileReader.Models;
using System;
using System.Linq;

namespace FileReader.Classes
{
    class MenuHelper
    {
        private DirectoryReader _dirReader;
        private readonly Repository _repository;
        private readonly FileFacade _fileFacade;
        private readonly ConsolePrinter _printer;
        private readonly InputHelper _inputHelper;
        

        public MenuHelper(Repository repository, FileFacade fileFacade, MenuNumValidator numValidator, ConsolePrinter printer,InputHelper inputHelper)
        {
            _dirReader = new DirectoryReader();
            _repository = repository;
            _fileFacade = fileFacade;
            _printer = printer;
            _inputHelper = inputHelper;
        }



        public bool ShowDirsAndFiles(string dirPath = null)
        {
            bool data = false;
            _dirReader.IsErrorRead = false;
            _repository.DirsList.Clear();
            _repository.FileList.Clear();


            _repository.BasePath = (string.IsNullOrEmpty(dirPath)) ? @"C:\" : dirPath;
            _repository.DirsList = (string.IsNullOrEmpty(dirPath)) ? _dirReader.Read(@"C:\") : _dirReader.Read(dirPath);

            if(_repository.DirsList.Count == 0)
            {
                if (_dirReader.IsErrorRead) return false;
            }

            _repository.FileList = _fileFacade.GetListFiles();



            if (_repository.DirsList.Count > 0 || _repository.FileList.Count > 0)
            {
                if (_repository.DirsList.Count > 0)
                {
                    Console.WriteLine("Доступные папки в " + _repository.BasePath);

                    foreach (var currDir in _repository.DirsList)
                    {
                        Console.WriteLine(currDir.DirNum + ") " + currDir.Dir.Name);
                    }

                    Console.WriteLine();
                }


                foreach (var file in _repository.FileList)
                {
                    Console.WriteLine("---- " + file.FileNum +")" + file.FilePath);
                }

                data = true;
            }
            else
            {
                Console.WriteLine("No data ... \n\n");
                data = false;
            }
            return data;
        }


 



        public DirectoryLocation SelectDir(string nChoice)
        {
            int choiceNum = int.Parse(nChoice);
            return _repository.DirsList.Where(n => n.DirNum == choiceNum).First();
        }



        public FileLocation SelectFile(string nChoice)
        {
            int choiceNum = int.Parse(nChoice);
            return _repository.FileList.Where(n => n.FileNum == choiceNum).First();
        }




        public void ShowFile()
        {
            if (_repository.FileList.Count > 0)
            {
                _inputHelper.InputFile();

                if (_inputHelper.FileNum != "0")
                {
                    var selectedFileName = SelectFile(_inputHelper.FileNum);
                    var data = _fileFacade.ReadFile(selectedFileName.FilePath);

                    if (data == null) return;
                    if (data.Length != 0)
                    {
                        _printer.Print(data);
                    }
                    else {
                        Console.WriteLine("\nFile is empty ... ");
                    }

                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Ошибка! Отсутствуют файлы для чтения!");
            }
        }




        public void ShowAdditionalCommands()
        {
            if (_repository.BasePath != @"C:\")
            {
                Console.WriteLine("\nВведите \"0\" чтобы вернуться назад >>");
            }

            if (_repository.FileList.Count > 0)
            {
                Console.WriteLine("Введите \"r\" чтобы прочитать файл >>>");
            }

            Console.WriteLine("Введите \"q\" чтобы выйти >>>");
        }



        public bool IsFilesWithCurrentExtExists()
        {
            return (_repository.FileList.Count > 0) ? true : false;
        }


        public void ShowExt()
        {
            Console.WriteLine("Select file extention mode : .dat | .info | .txt | *");
        }

    }
}

