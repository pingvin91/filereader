﻿using FileReader.Common.BaseCode;
using FileReader.Infrastructure.Classes;
using System.Collections.ObjectModel;


namespace FileReader.Wpf.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {

        #region fields
        private Facade _app;
        private ObservableCollection<FileSystemObjectInfo> _tree;
        private ObservableCollection<string> _folderList;

        #endregion

        #region properties
        public ObservableCollection<FileSystemObjectInfo> Tree
        {
            get { return _tree; }
            set {
                _tree = value;
                OnPropertyChanged(nameof(Tree));
            }
        }


        public ObservableCollection<string> FolderList
        {
            get { return _folderList; }
            set
            {
                _folderList = value;
                OnPropertyChanged(nameof(FolderList));
            }
        }
       

        #endregion


        public MainWindowViewModel()
        {
            Initialize();
        }


        private void Initialize()
        {
            _app = new Facade();
            _tree = new ObservableCollection<FileSystemObjectInfo>(_app.GetTree());
            _folderList = new ObservableCollection<string>();
        }

    }
}
