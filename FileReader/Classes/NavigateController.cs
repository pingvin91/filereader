﻿using System;

namespace FileReader.Classes
{
    class NavigateController
    {
        private MenuHelper _menuHelper;
        private string _oldFullPath;
        private PathParser _pathParser;
        private readonly Repository _repo;
        private readonly InputHelper _inputHelper;


        public NavigateController(MenuHelper menuHelper, Repository repo, InputHelper inputHelper)
        {
           
            _menuHelper = menuHelper;
            _pathParser = new PathParser();
            _repo = repo;
            _oldFullPath = @"C:\";
            _inputHelper = inputHelper;
        }


        public void Navigate()
        {
            bool res;
            _menuHelper.ShowDirsAndFiles();

            do
            {
                if (_repo.DirsList.Count > 0)
                {
                    Console.WriteLine("\nВведите номер директории для чтения данных >>>\n");

                    _menuHelper.ShowAdditionalCommands();
                    _inputHelper.InputDir();

                    if(!IsDirZeroAndReturn())
                    {
                        var fullDirPath = _menuHelper.SelectDir(_inputHelper.DirNum).Dir.FullName;
                        Console.WriteLine("Выбрана директория: " + fullDirPath);

                        res = _menuHelper.ShowDirsAndFiles(fullDirPath);
                        TryReturnToOldDir(res, fullDirPath);
                        NavigateToFiles();
                    }
                }
                else
                {
                    _menuHelper.ShowAdditionalCommands();
                    _inputHelper.InputDir();
                    IsDirZeroAndReturn();
                }

            } while (true);
        }



        private void NavigateToFiles()
        {
            if (_menuHelper.IsFilesWithCurrentExtExists())
            {
                string inp = string.Empty;
                do {
                    _menuHelper.ShowAdditionalCommands();
                    inp = Console.ReadLine();
                    ValidateSpecChars(inp);

                } while (inp != "q" || inp != "0" || inp != "r");
            }
        }




        private bool IsDirZeroAndReturn()
        {
            if (_inputHelper.DirNum == "0")
            {
                _repo.BasePath = @"C:\";
                Navigate();
                return true;
            }

            return false;
        }




        private void TryReturnToOldDir(bool res, string fullDirPath)
        {
            if (!res)
            {
                _menuHelper.ShowDirsAndFiles(_oldFullPath);
            }
            else
            {
               
                _oldFullPath = fullDirPath;
            }
        }




        private void ValidateSpecChars(string inp)
        {
            if (inp == "q")
            {
                Environment.Exit(0);
            }
            else if (inp == "r")
            {
                _menuHelper.ShowFile();
                _menuHelper.ShowDirsAndFiles(_oldFullPath);
            }
            else if (inp == "0")
            {
                _repo.BasePath = @"C:\";
                Navigate();
            }
            else
            {
                Console.WriteLine("Повторите ввод ... \n");
            }

        }
    }
}
