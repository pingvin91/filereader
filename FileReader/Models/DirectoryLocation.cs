﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader.Models
{
    class DirectoryLocation
    {
        public int DirNum { get; set; }
        public DirectoryInfo Dir { get; set; }
    }
}
