﻿using System;
using System.Collections.Generic;
using System.Linq;
using FileReader.Models;
using System.IO;

namespace FileReader.Classes
{
    class DirectoryReader
    {
        public bool IsErrorRead { get; set; } 



        public List<DirectoryLocation> Read(string dirPath)
        {
            var locate = new List<DirectoryLocation>();

            if (string.IsNullOrEmpty(dirPath)) throw new Exception("Exception in read directories !!! ");

            DirectoryInfo dir = new DirectoryInfo(dirPath);
            try
            {
                int pos = 1;
                var dirList = dir.GetDirectories().OrderBy(x => x.Name).ToList();
                


                foreach (var currDir in dirList)
                {
                    locate.Add(new DirectoryLocation { DirNum = pos, Dir = currDir });
                    pos++;
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("Ошибка доступа к данным. Выберите другую дирректорию для чтения файлов ...");
                IsErrorRead = true;
            }

            return locate;
        }
    }
}
