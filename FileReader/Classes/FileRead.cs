﻿using FileReader.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileReader.Classes
{
    class FileRead
    {
        public List<FileLocation> ReadExistListFiles(string filePath, string extention)
        {
            if (string.IsNullOrEmpty(filePath)) throw new Exception("Exception in read files !!! ");
            string[] listFiles;

            if (extention == "*")
            {
                listFiles = Directory.GetFiles(filePath, "*.*")
                                     .Where(s =>
                                            s.EndsWith(".dat") ||
                                            s.EndsWith(".txt") ||
                                            s.EndsWith(".info"))
                                             .ToArray();
            }
            else
            {
                listFiles = Directory.GetFiles(filePath, "*." + extention);
            }
            var listFilesWithNum = new List<FileLocation>();


            int pos = 1;

            foreach (var file in listFiles)
            {
                listFilesWithNum.Add(new FileLocation { FileNum = pos, FilePath = file});
                pos++;
            }

            return listFilesWithNum;
        }


        public string[] Read(string selectedFile)
        {
            string[] argv = { };
            try
            {
                argv = File.ReadAllLines(selectedFile);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Ошибка доступа! Невозможно прочитать выбранный файл ...");
                return null;
            }
            return argv;
        }
    }
}
