﻿
using System.Collections.Generic;


namespace FileReader.Infrastructure.Classes
{
    public class Facade
    {

        private readonly TreeProvider _treeProvider;

        public Facade()
        {
            _treeProvider = new TreeProvider();
        }


        public List<FileSystemObjectInfo> GetTree()
        {
            return _treeProvider.ProvideTree();
        }

    }
}
