﻿namespace FileReader.Infrastructure.Enums
{
    public enum ItemState : short
    {
        Undefined,
        Open,
        Close
    }
}
