﻿
namespace FileReader.Enums
{
    public enum FileExt
    {
        dat = 0,
        info = 1,
        txt = 2,
        all = 3
    }
}
