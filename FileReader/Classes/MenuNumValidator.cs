﻿namespace FileReader.Classes
{
    class MenuNumValidator
    {
        public bool IsValid(string choseNum)
        {
            int nLocate;
            if (int.TryParse(choseNum, out nLocate))
            {
                if(nLocate < 0) return false;

                return true;
            }

            return false;
        }
    }
}
