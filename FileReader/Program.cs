﻿using FileReader.Classes;
using System;

namespace FileReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var userFacade = new UserFacade();
            userFacade.ShowMenu();
            Console.ReadLine();
        }
    }
}
