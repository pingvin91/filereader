﻿namespace FileReader.Models
{
    class FileLocation
    {
        public int FileNum { get; set; }
        public string FilePath { get; set; }
    }
}
