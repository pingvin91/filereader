﻿using System;

namespace FileReader.Classes
{
    class ExtPicker
    {
        private readonly MenuHelper _menuHelper;
        private readonly Repository _repo;
        public ExtPicker(MenuHelper menuHelper, Repository repo)
        {
            _menuHelper = menuHelper;
            _repo = repo;
        }

        public string GetExt()
        {
            _menuHelper.ShowExt();
            return Console.ReadLine();
        }

        public void SetExt(string extValue)
        {
            _repo.CurrExt = extValue;
            Console.WriteLine("Выбрано расширение файла: " + extValue);
        }
    }
}
