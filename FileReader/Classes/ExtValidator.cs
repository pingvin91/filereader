﻿using FileReader.Enums;

namespace FileReader.Classes
{
    class ExtValidator
    {
        public bool IsValid(string ext)
        {
            if (ext == FileExt.dat.ToString())
                return true;
            if (ext == FileExt.info.ToString())
                return true;
            if (ext == FileExt.txt.ToString())
                return true;
            if (ext == "*")
                return true;

            return false;
        }
    }
}
