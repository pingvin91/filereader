﻿using System.Collections.Generic;
using System.IO;

namespace FileReader.Infrastructure.Classes
{
    public class TreeProvider
    {
        public List<FileSystemObjectInfo> ProvideTree()
        {
            var tree = new List<FileSystemObjectInfo>();
            var drives = DriveInfo.GetDrives();

            foreach (var drive in drives)
            {
                tree.Add(new FileSystemObjectInfo(drive));
            }

            return tree;
        }
    }
}
