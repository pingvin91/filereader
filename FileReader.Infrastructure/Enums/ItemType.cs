﻿namespace FileReader.Infrastructure.Enums
{
    public enum ItemType : short
    {
        Folder,
        File
    }
}
