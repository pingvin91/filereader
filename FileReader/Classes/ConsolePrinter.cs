﻿using System;
using System.Text;

namespace FileReader.Classes
{
    public class ConsolePrinter
    {
        public ConsolePrinter()
        {
            Console.OutputEncoding = Encoding.UTF8;
        }

        public void Print(string text)
        {
            if (string.IsNullOrEmpty(text)) return;
            Console.WriteLine(text);
        }


        public void Print(string [] lines)
        {
            if (lines.Length == 0) return;

            for (var i = 0; i < lines.Length; i++)
            {
                Console.WriteLine(lines[i]);
            }
           
        }
    }
}
