﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader.Classes
{
    public class UserFacade
    {
        private Repository _repository;
        private ConsolePrinter _consolePrinter;
        private FileFacade _fileFacade;
        private MenuNumValidator _numValidator;
        private InputHelper _inputHelper;
        /// <summary>
        /// 
        /// </summary>


        private ExtValidator _extValidator;
        private NavigateController _controller;
        private MenuHelper _menuHelper;
        private ExtPicker _modePicker;



        public UserFacade()
        {
            _consolePrinter = new ConsolePrinter();
            _numValidator = new MenuNumValidator();
            _repository = new Repository();
            _extValidator = new ExtValidator();
            _fileFacade = new FileFacade(_repository);
            _inputHelper = new InputHelper(_repository);
            _menuHelper = new MenuHelper(_repository, _fileFacade, _numValidator, _consolePrinter, _inputHelper);
            _controller = new NavigateController(_menuHelper, _repository,_inputHelper);
            _modePicker = new ExtPicker(_menuHelper, _repository);
        }


        
        public void ShowMenu()
        {
            string mode = string.Empty;

            do
            {
                mode = _modePicker.GetExt();

                if (!_extValidator.IsValid(mode))
                {
                    Console.WriteLine("Некорректный выбор расширения файл. Повторите попытку");
                }

            } while (!_extValidator.IsValid(mode));

            // Сохраняем выбранное рсширение
            _modePicker.SetExt(mode);
            _controller.Navigate();
        }
    }
}


