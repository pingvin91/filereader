﻿using FileReader.Models;
using System.Collections.Generic;


namespace FileReader.Classes
{
    class FileFacade
    {    
        private readonly FileRead _fileReader;
        private readonly Repository _repos;

        public FileFacade(Repository repos)
        {
            _repos = repos;
            _fileReader = new FileRead();
        }


        public string[] ReadFile(string path)
        {
            return _fileReader.Read(path);
        }

        public List<FileLocation> GetListFiles()
        {
            return _fileReader.ReadExistListFiles(_repos.BasePath, _repos.CurrExt);
        }
    }
}
