﻿using System;

namespace FileReader.Classes
{
    public class InputHelper
    {
        public string DirNum { get; set; }
        public string FileNum { get; set; }

        private readonly Repository _repo;
        private MenuNumValidator _numValidator;

        public InputHelper(Repository repo)
        {
            _numValidator = new MenuNumValidator();
            _repo = repo;
        }




        public void InputDir()
        {
            do
            {
                // Вводим номер директории для выбора из предложенного списка
                DirNum = Console.ReadLine();

                if (DirNum == "q") Environment.Exit(0);
                if (DirNum == "0") break;

                if (!_numValidator.IsValid(DirNum) || int.Parse(DirNum) > _repo.DirsList.Count)
                {
                    Console.WriteLine("Некорректный выбор номера директории");
                }

            } while (!_numValidator.IsValid(DirNum) || int.Parse(DirNum) > _repo.DirsList.Count);

        }






        public void InputFile()
        {
            do
            {
                Console.WriteLine("Введите номер файла чтобы прочитать данные ....");

                FileNum = Console.ReadLine();

                if (FileNum == "q") Environment.Exit(0);

                if (!_numValidator.IsValid(FileNum) || int.Parse(FileNum) > _repo.FileList.Count )
                {
                    Console.WriteLine("Некорректный выбор номера файла для чтения");
                }

            } while (!_numValidator.IsValid(FileNum) || int.Parse(FileNum) > _repo.FileList.Count);

        }
    }
}
