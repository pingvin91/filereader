﻿using FileReader.Common.BaseCode;
using FileReader.Infrastructure.Enums;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Media;

namespace FileReader.Infrastructure.Classes
{
    public class FileSystemObjectInfo : BaseObject
    {

        #region private fields

        private ObservableCollection<FileSystemObjectInfo> _children;
        private ImageSource _imageSource;
        private bool _isExpanded;
        private DriveInfo _drive;
        private FileSystemInfo _fileSystemInfo;

        #endregion



        #region  Constructors

        public FileSystemObjectInfo(FileSystemInfo info)
        {
            if (this is DummyFileSystemObjectInfo) return;

            Children = new ObservableCollection<FileSystemObjectInfo>();

            FileSystemInfo = info;

            if (info is DirectoryInfo)
            {
                ImageSource = FolderManager.GetImageSource(info.FullName, ItemState.Close);
                AddDummy();
            }
            else if (info is FileInfo)
            {
                ImageSource = FileManager.GetImageSource(info.FullName);
            }
            PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(FileSystemObjectInfo_PropertyChanged);
        }

        public FileSystemObjectInfo(DriveInfo drive)
            : this(drive.RootDirectory)
        {
            Drive = drive;
        }

        #endregion







        #region Properties

        public ObservableCollection<FileSystemObjectInfo> Children
        {
            get { return _children; }
            private set
            {
                if (_children == value) return;
               
                _children = value;
                OnPropertyChanged(nameof(Children));
            }
        }



        public ImageSource ImageSource
        {
            get { return _imageSource; }
            private set{

            if (_imageSource == value) return;
               
                _imageSource =  value;
                OnPropertyChanged(nameof(ImageSource));
            }
        }



        public bool IsExpanded
        {
            get { return _isExpanded; }
            set {
                if (_isExpanded == value) return;

                _isExpanded = value;
                OnPropertyChanged(nameof(IsExpanded));
            }
        }



        public FileSystemInfo FileSystemInfo
        {
            get { return _fileSystemInfo; }
            private set {
                if (_fileSystemInfo == value) return;
                _fileSystemInfo = value;
                OnPropertyChanged(nameof(FileSystemInfo));
            }
        }





        private DriveInfo Drive
        {
            get { return _drive; }
            set {
                if (_drive == value) return; 
                _drive = value;
                OnPropertyChanged(nameof(Drive));
            }
        }

        #endregion








        #region Private methods

        private void AddDummy()
        {
            Children.Add(new DummyFileSystemObjectInfo());
        }



        private bool HasDummy()
        {
            return !ReferenceEquals(GetDummy(), null);
        }



        private DummyFileSystemObjectInfo GetDummy()
        {
            var list = Children.OfType<DummyFileSystemObjectInfo>().ToList();
            if (list.Count > 0) return list.First();
            return null;
        }



        private void RemoveDummy()
        {
            Children.Remove(GetDummy());
        }




        private void ExploreDirectories()
        {
            if (!ReferenceEquals(Drive, null))
            {
                if (!Drive.IsReady) return;
            }
            try
            {
                if (this.FileSystemInfo is DirectoryInfo)
                {
                    var directories = ((DirectoryInfo)FileSystemInfo).GetDirectories();
                    foreach (var directory in directories.OrderBy(d => d.Name))
                    {
                        if (!Equals((directory.Attributes & FileAttributes.System), FileAttributes.System) &&
                            !Equals((directory.Attributes & FileAttributes.Hidden), FileAttributes.Hidden))
                        {
                            Children.Add(new FileSystemObjectInfo(directory));
                        }
                    }
                }
            }
            catch
            {
                /*throw;*/
            }
        }



        private void ExploreFiles()
        {
            if (!ReferenceEquals(Drive, null))
            {
                if (!Drive.IsReady) return;
            }
            try
            {
                if (FileSystemInfo is DirectoryInfo)
                {
                    var files = ((DirectoryInfo) FileSystemInfo).GetFiles();


                  

                    foreach (var file in files.OrderBy(d => d.Name))
                    {
                        if (!Equals((file.Attributes & FileAttributes.System), FileAttributes.System) &&
                            !Equals((file.Attributes & FileAttributes.Hidden), FileAttributes.Hidden))
                        {
                            Children.Add(new FileSystemObjectInfo(file));
                        }
                    }
                }
            }
            catch
            {
                /*throw;*/
            }
        }


      




        private void FileSystemObjectInfo_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (FileSystemInfo is DirectoryInfo)
            {
                if (string.Equals(e.PropertyName, "IsExpanded", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (IsExpanded)
                    {
                        ImageSource = FolderManager.GetImageSource(this.FileSystemInfo.FullName, ItemState.Open);

                        if (HasDummy())
                        {
                            RemoveDummy();
                            ExploreDirectories();
                            ExploreFiles();
                        }
                    }
                    else
                    {
                        ImageSource = FolderManager.GetImageSource(FileSystemInfo.FullName, ItemState.Close);
                    }
                }
            }
        }


        #endregion

        private class DummyFileSystemObjectInfo : FileSystemObjectInfo
        {
            public DummyFileSystemObjectInfo()
                : base(new DirectoryInfo("DummyFileSystemObjectInfo"))
            {
            }
        }


    }
}

