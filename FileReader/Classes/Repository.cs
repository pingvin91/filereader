﻿using FileReader.Enums;
using FileReader.Models;
using System.Collections.Generic;

namespace FileReader.Classes
{
    public class Repository
    {
      
        public string SelectedFile { get; set; }

        public FileExt SelectedExt { get; set; }

        internal List<DirectoryLocation> DirsList { get; set; }
        internal List<FileLocation> FileList { get; set; }

        public string BasePath { get; set; }
        public string CurrExt { get; set; }

        public Repository()
        {
            DirsList = new List<DirectoryLocation>();
            FileList = new List<FileLocation>();
        }


     

    }
}
